<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Blog;
use Faker\Generator as Faker;

$factory->define(Blog::class, function (Faker $faker) {
    return [
        'title' => $faker->sentence(5),
        'content' => $faker->paragraph,
        'slug' => $faker->sentence(5),
        'user_id' => factory(App\User::class),
        'photo' => $faker->imageUrl(640, 480, 'cats'),
        'categories_id' => '2'
    ];
});
