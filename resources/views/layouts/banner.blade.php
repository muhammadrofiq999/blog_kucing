<!-- banner -->
<div class="banner about-banner">
    <div class="header about-header">
        <div class="container">
            <div class="header-left">
                <div class="w3layouts-logo">
                    <h1>
                        <a href="{{ asset('/templeteBlogKucing/index.html')}}">Cat <span>Club</span></a>
                    </h1>
                </div>
            </div>
            <div class="header-right">
                <div class="top-nav">
                    <nav class="navbar navbar-default">
                            <div class="navbar-header">
                                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                                    <span class="sr-only">Toggle navigation</span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>
                            </div>
                        <!-- Collect the nav links, forms, and other content for toggling -->
                        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                            <ul class="nav navbar-nav">
                                <li><a href="/">Home</a></li>
                                <li><a href="{{ asset('/templeteBlogKucing/gallery.html')}}">Gallery</a></li>
                                <li>
                                    @guest
                                    <!-- Desktop Button -->
                                    <form class="form-inline">
                                        <button style="margin-top: -7px; color: black" class="btn btn-login btn-navbar-right" type="button"
                                            onclick="event.preventDefault(); location.href='{{ url('login')}}';">
                                            Login
                                        </button>
                                    </form>
                                    @endguest

                                    @auth
                                    <!-- Desktop Button -->
                                    <div class="d-flex flex-row">
                                        <div>
                                            <form class="form-inline" action="" method="GET">
                                                @csrf
                                                <a style="margin-top: -7px; color: black" class="btn btn-login btn-navbar-right " type="submit">
                                                    Selamat datang <span style="font-weight: 900; color: cadetblue" class="mr-2">{{ Auth::user()->name }}</span>
                                                </a>
                                            </form>
                                        </div>
                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            @csrf
                                        </form>
                                    </div>
                                    @endauth
                                </li>
                                <li>
                                    @auth
                                    <!-- Desktop Button -->
                                    <div class="d-flex flex-row">
                                        <div>
                                            <a style="margin-top: -7px; color: black" class="btn" href="{{ route('logout') }}" onclick="event.preventDefault();
                                                document.getElementById('logout-form').submit();">
                                                log out
                                            </a>
                                        </div>
                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            @csrf
                                        </form>
                                    </div>
                                    @endauth
                                </li>

                            </ul>
                            <div class="clearfix">

                            </div>
                        </div>
                    </nav>
                </div>
                <div class="agileinfo-social-grids">
                    <ul>
                        <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                        <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                        <li><a href="#"><i class="fa fa-rss"></i></a></li>
                        <li><a href="#"><i class="fa fa-vk"></i></a></li>
                    </ul>
                </div>
                <div class="clearfix"> </div>
            </div>
            <div class="clearfix"> </div>
        </div>
    </div>
    <div class="about-heading">
        <div class="container">
            <h2>Our Blog</h2>
        </div>
    </div>
</div>
<!-- //banner -->
