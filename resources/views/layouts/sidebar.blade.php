<div class="col-md-4 blog-top-right-grid">
    <div class="Categories">
        <h3>Categories</h3>
        <ul>
            {{-- <li><a href="#">Phasellus sem leo, interdum quis risus</a></li>
            <li><a href="#">Nullam egestas nisi id malesuada aliquet </a></li>
            <li><a href="#"> Donec condimentum purus urna venenatis</a></li>
            <li><a href="#">Ut congue, nisl id tincidunt lobor mollis</a></li>
            <li><a href="#">Cum sociis natoque penatibus et magnis</a></li>
            <li><a href="#">Suspendisse nec magna id ex pretium</a></li> --}}

            @foreach ($category as $ct)
                <li><a href="#">{{$ct->name}}</a></li>
            @endforeach

        </ul>
    </div>
    <div class="Categories">
        <h3>Latest </h3>
        <ul class="marked-list offs1">
            @foreach ($latest as $latests)
                <li><a href="/blogs/{{$latests->slug}}">{{$latests->title}}</a></li>
            @endforeach
            {{-- <li><a href="#">May 2015 (7)</a></li>
            <li><a href="#">April 2015 (11)</a></li>
            <li><a href="#">March 2015 (12)</a></li>
            <li><a href="#">February 2015 (14)</a> </li>
            <li><a href="#">January 2015 (10)</a></li>
            <li><a href="#">December 2014 (12)</a></li>
            <li><a href="#">November 2014 (8)</a></li>
            <li><a href="#">October 2014 (7)</a> </li>
            <li><a href="#">September 2014 (8)</a></li>
            <li><a href="#">August 2014 (6)</a></li> --}}
        </ul>
    </div>
    <div class="comments">
        <h3>Recent Comments</h3>
        @foreach ($comment as $item)
        <div class="comments-text">
                <h3>{{$item->users->name}}</h3>
                <p>{{$item->content}}</p>
                <p>{{date('F d,Y H:i', strtotime($item->created_at))}}</p>
            <div class="clearfix"> </div>
        </div>
        @endforeach
      
</div>
<div class="clearfix"></div>
</div>

</div>