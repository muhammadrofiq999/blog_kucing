@extends('admin.master')
@push('css')
<link rel="stylesheet" href="{{ asset('admin')}}/plugins/summernote/summernote-bs4.css">
@endpush
@section('pages','Edit Blog')
@section('content')
<div class="col-md-12">
  <div class="card card-outline card-info">
    <div class="card-header">
      <h3 class="card-title">
        Create New Blog
      </h3>
    </div>
    <!-- /.card-header -->
    <div class="card-body pad">
      <form action="/blog/{{$blog->id}}" method="post" enctype="multipart/form-data">
        @csrf
        @method('PUT')
        <div class="form-group">
          <label for="exampleFormControlInput1">Title</label>
          <input type="text" class="form-control" name="title" value="{{$blog->title}}" placeholder="Insert Blog Title...">
        </div>
        <div class="form-group">
          <label for="exampleFormControlSelect1">Category</label>
          <select class="form-control" name="categories">
            <option>--Select Category--</option>
            @foreach ($category as $categories)
            <option value="{{$categories->id}}" {{($categories->id == $blog->categories_id) ? 'selected':''}}>{{$categories->name}}</option>
            @endforeach
          </select>
        </div>
        <div class="mb-3">
          <label>Content</label>
          <textarea class="textarea" placeholder="Place some text here"  name="content">{{$blog->content}}</textarea>
        </div>
        <div class="form-group mb-3">
          <label>Photo</label>
          <input type="file" name="photo" class="form-control" id="inputGroupFile01">
        </div>
    </div>
    <div class="form-group px-2 py-3">
      <input type="submit" value="Save" class="btn btn-primary float-right">
    </div>
    </form>
  </div>
</div>
</div>

<!-- /.col-->
@endsection
@push('script')
<script src="{{asset('admin')}}/plugins/summernote/summernote-bs4.min.js"></script>
<script>
  $(function() {
    // Summernote
    $('.textarea').summernote()
  })
</script>
@endpush