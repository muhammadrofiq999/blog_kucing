@extends('admin.master')
@section('pages','Profile')
@section('content')
    <!-- general form elements -->
    <div class="card card-primary">
        <div class="card-header">
          <h3 class="card-title">Profile</h3>
        </div>
        <!-- /.card-header -->
        <!-- form start -->
        <form action="/profile/update" method="POST">
        @csrf
          <div class="card-body">
            <div class="form-group">
                <label>Name</label>
                <input type="text" class="form-control" name="name" value="{{$profile->name}}" placeholder="Enter Name">
              </div>
            <div class="form-group">
              <label for="exampleInputEmail1">Email address</label>
              <input type="email" class="form-control" name="email" value="{{$profile->email}}" placeholder="Enter email">
            </div>
            <div class="form-group">
              <label for="exampleInputPassword1">New Password</label>
              <input type="password" class="form-control" name="password" placeholder="Password">
            </div>
          </div>
          <!-- /.card-body -->
          <div class="card-footer">
            <button type="submit" class="btn btn-primary float-right">Save</button>
          </div>
        </form>
      </div>
      <!-- /.card -->
@endsection