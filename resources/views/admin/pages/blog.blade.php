@extends('admin.master')
@push('css')
<link href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css" rel="stylesheet">
<link href="https://cdn.datatables.net/1.10.21/css/dataTables.bootstrap4.min.css" rel="stylesheet">
@endpush
@section('pages','Blog')
@section('content')
<div class="card">
  <div class="card-header"><a href="{{route('blog.create')}}" class="btn btn-primary">New Blog</a></div>
  <div class="card-body">
      <table class="table table-bordered datatables">
        <thead>                  
          <tr>
            <th style="width: 10px">#</th>
            <th>Category</th>
            <th>Author</th>
            <th>Title</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
     {{-- @forelse ($blog as $index=>$blogs)
     <tr>
      <td>{{ $index + 1 }}</td>
      <td>{{$blogs->categories->name}}</td>
      <td>{{ $blogs->users->name}}</td>
      <td>{{ $blogs->title }}</td>
      <td>
          <form action="{{ route('blog.destroy', $blogs->id) }}" method="POST">
              @csrf
              @method('delete')
              <a href="{{route('blog.edit',$blogs->id)}}" style="margin-right: 20px"><i
                      class="fas fa-edit"></i></a>
              <button style="border: none;
                      background: none;
                      "><i class="fas fa-trash"></i></button>
          </form>
      </td>
  </tr>
     @empty
         <tr> <td class="text-center" style="background-color:rgba(221, 221, 221, 0.644);" colspan="3"><h3>No Data</h3></td></tr>
     @endforelse --}}
        </tbody>
      </table>
    </div>
</div>
@endsection
@push('script')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>  
<script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.21/js/dataTables.bootstrap4.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
<script type="text/javascript">
  $(function () {
    $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
    });
    var table = $('.datatables').DataTable({
        processing: false,
        serverSide: true,
        ajax: "{{ route('blog.index') }}",
        columns: [
            {data: 'DT_RowIndex', name: 'DT_RowIndex'},
            {data: 'categories.name', name: 'categories'},
            {data:'users.name',name:'author'},
            {data: 'title', name: 'title'},
            {
                data: 'action', 
                name: 'action', 
                orderable: true, 
                searchable: true
            },
        ]
    });
    $('body').on('click','.delete',function(){
      var blog_id = $(this).data('id');
      Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.isConfirmed) {
      $.ajax({
            type: "DELETE",
            url: "/blog/"+blog_id,
            success: function (data) {
                table.draw();
                Swal.fire(
              'Deleted!',
              'Your blog has been deleted.',
              'success'
            )
            },
            error: function (data) {
                console.log('Error:', data);
            }
        });
      
  
       }
})
    });
// $.get( "{{route('blog.index')}}", function( data ) {
//   console.log(data);;
//   alert( "Load was performed." );
// });
  });
</script>
@endpush