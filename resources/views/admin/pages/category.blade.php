@extends('admin.master')
@section('pages','Category')
@section('content')
<div class="card">
    <div class="card-header"><button class="btn btn-primary" data-toggle="modal" data-target="#createmodal">New Category</button></div>
    <div class="card-body">
        <table class="table table-bordered">
          <thead>                  
            <tr>
              <th style="width: 10px">#</th>
              <th>Name</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
       @forelse ($category as $index=>$categories)
       <tr>
        <td>{{ $index + 1 }}</td>
        <td>{{ $categories->name }}</td>
        <td>
            <form action="{{ route('category.destroy', $categories->id) }}" method="POST">
                @csrf
                @method('delete')
                <a href="javascript:;" id="edit_btn" data-id="{{ $categories->id }}"
                    data-name="{{ $categories->name }}" style="margin-right: 20px"><i
                        class="fas fa-edit"></i></a>
                <button style="border: none;
                        background: none;
                        "><i class="fas fa-trash"></i></button>
            </form>
        </td>
    </tr>
       @empty
           <tr> <td class="text-center" style="background-color:rgba(221, 221, 221, 0.644);" colspan="3"><h3>No Data</h3></td></tr>
       @endforelse
          </tbody>
        </table>
      </div>
</div>
{{-- modal --}}
<div class="modal fade" id="createmodal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <form action="{{ route('category.store') }}" method="post">
            @csrf  
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <div class="form-group">
                <label for="name">Name Category</label>
                <input type="text" name="name_category" class="form-control"
                    placeholder="Insert Category Name...">
            </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary">Add Category</button>
        </div>
    </form>
      </div>
    </div>
  </div>
  {{-- modal edit --}}
  <div class="modal fade" id="editmodal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <form action="{{ route('category.store') }}" method="post">
        @csrf
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Edit Categories</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <input type="hidden" name="id" id="id">
                        <label for="name">Name Categories</label>
                        <input type="text" name="name_category" id="name_categories" class="form-control" value="">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save changes</button>
                </div>
            </div>
    </form>
</div>
</div>
@endsection
@push('script')
    <script>
        $(document).ready(function() {
            $("table #edit_btn").click(function() {
                var id = $(this).data('id');
                var name = $(this).data('name');
                $('#editmodal').modal('show');
                $('#id').val(id);
                $('#name_categories').val(name);
            })
        });

    </script>
@endpush
