<!--A Design by W3layouts
Author: W3layout
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE html>
<html lang="en">
<head>
<title>Kucingku Sayang</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta charset="utf-8">
<meta name="keywords" content="Cat Club Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template,
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<!-- bootstrap-css -->
<link href="{{ asset('/templeteBlogKucing/css/bootstrap.css')}}" rel="stylesheet" type="text/css" media="all" />
<!--// bootstrap-css -->
<!-- css -->
<link rel="stylesheet" href="{{ asset('/templeteBlogKucing/css/style.css')}}" type="text/css" media="all" />
<!--// css -->
<!-- font-awesome icons -->
<link href="{{ asset('/templeteBlogKucing/css/font-awesome.css')}}" rel="stylesheet">
<!-- //font-awesome icons -->
<!-- font -->
<link href='//fonts.googleapis.com/css?family=Pacifico' rel='stylesheet' type='text/css'>
<link href="//fonts.googleapis.com/css?family=Raleway:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
<link href='//fonts.googleapis.com/css?family=Roboto+Condensed:400,700italic,700,400italic,300italic,300' rel='stylesheet' type='text/css'>
<!-- //font -->
<script src="{{ asset('/templeteBlogKucing/js/jquery-1.11.1.min.js')}}"></script>
<script src="{{ asset('/templeteBlogKucing/js/bootstrap.js')}}"></script>
<script type="{{ asset('/templeteBlogKucing/text/javascript')}}">
	jQuery(document).ready(function($) {
		$(".scroll").click(function(event){
			event.preventDefault();
			$('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
		});
	});
</script>
<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<![endif]-->
</head>
<body>
@include('layouts.banner')
	<!-- blog -->
	<div class="blog">
		<!-- container -->
		<div class="container">
			<div class="blog-top-grids">
				<div class="col-md-8 blog-top-left-grid">
					<div class="left-blog">
						{{-- <div class="blog-left"> --}}
							{{-- <div class="blog-left-right">
								<a href="{{ asset('/templeteBlogKucing/single.html')}}">Kucing Kampung</a>
								<p>
									Kucing yang hidup di lingkungan manusia. Mereka dapat dilihat dimana-mana. <br>
									Memiliki bulu yang sedang dan warna yang beragam. <br>
									Cocok untuk dipelihara jika memiliki budget pas-pasan.
							</p>
							</div>
							<div class="clearfix"> </div>
						</div> --}}
						{{-- <div class="blog-left">
							<div class="blog-left-left">
								<p>Posted By <a href="#">Admin</a> &nbsp;&nbsp; on June 2, 2015 &nbsp;&nbsp; <a href="#">Comments (10)</a></p>
								<a href="{{ asset('/templeteBlogKucing/single.html')}}"><img src="{{ asset('/templeteBlogKucing/images/b2.jpg')}}" alt="" /></a>
							</div>
							<div class="blog-left-right wow fadeInRight animated animated" data-wow-delay=".5s">
								<a href="{{ asset('/templeteBlogKucing/single.html')}}">Kucing Orange</a>
								<p>Biasanya bar-bar tapi tetap menggemaskan.
								</p>
							</div>
							<div class="clearfix"> </div>
						</div> --}}
						@foreach ($blog as $blogs)
						<div class="blog-left">
							<div class="blog-left-left">
								<p>Posted By <a href="#">{{$blogs->users->name}}</a> &nbsp;&nbsp; on {{date('F d,Y', strtotime($blogs->created_at))}} &nbsp;&nbsp; <a href="#">Comments ({{count($blogs->comments)}})</a></p>
								<a href="/blogs/{{$blogs->slug}}"><img src="{{ asset('img').'/'.$blogs->photo}}" style="max-height: 500px" alt="" /></a>
							</div>
							<div class="blog-left-right">
								<a href="/blogs/{{$blogs->slug}}">{{$blogs->title}}</a>
								<div> <?= Str::limit($blogs->content, 1000, '...') ?>
									{{-- {!!($blogs->content)!!} --}}
								</div>
							</div>
							<div class="clearfix"> </div>
						</div>
						@endforeach
				
					</div>
					<nav>
						<ul class="pagination">
							<li>
								<a href="#" aria-label="Previous">
									<span aria-hidden="true">«</span>
								</a>
							</li>
							<li><a href="#">1</a></li>
							<li><a href="#">2</a></li>
							<li><a href="#">3</a></li>
							<li><a href="#">4</a></li>
							<li><a href="#">5</a></li>
							<li>
								<a href="#" aria-label="Next">
									<span aria-hidden="true">»</span>
								</a>
							</li>
						</ul>
					</nav>
				</div>
				
				@include('layouts.sidebar')
		<!-- //container -->
	</div>
	<!-- //blog -->
	<!-- footer -->
	@include('layouts.footer')
	<!-- footer -->
	<!-- copyright -->
	<div class="copyright">
		<div class="container">
			<p>© 2017 Cat Club. All rights reserved | Design by <a href="http://w3layouts.com">W3layouts</a></p>
		</div>
	</div>
	<!-- //copyright -->
	<script src="{{ asset('/templeteBlogKucing/js/SmoothScroll.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('/templeteBlogKucing/js/move-top.js')}}"></script>
<script type="text/javascript" src="{{ asset('/templeteBlogKucing/js/easing.js')}}"></script>
	<!-- here stars scrolling icon -->
	<script type="text/javascript">
		$(document).ready(function() {
			/*
				var defaults = {
				containerID: 'toTop', // fading element id
				containerHoverID: 'toTopHover', // fading element hover id
				scrollSpeed: 1200,
				easingType: 'linear'
				};
			*/

			$().UItoTop({ easingType: 'easeOutQuart' });

			});
	</script>
<!-- //here ends scrolling icon -->
</body>
</html>
