<!--A Design by W3layouts
Author: W3layout
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE html>
<html lang="en">
<head>
<title>Cat Club an animals and pets Category Bootstrap responsive Website Template | Single :: w3layouts</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta charset="utf-8">
<meta name="keywords" content="Cat Club Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<!-- bootstrap-css -->
<link href="{{asset('templeteBlogKucing')}}/css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
<!--// bootstrap-css -->
<!-- css -->
<link rel="stylesheet" href="{{asset('templeteBlogKucing')}}/css/style.css" type="text/css" media="all" />
<!--// css -->
<!-- font-awesome icons -->
<link href="{{asset('templeteBlogKucing')}}/css/font-awesome.css" rel="stylesheet"> 
<!-- //font-awesome icons -->
<link rel="stylesheet" href="{{asset('templeteBlogKucing')}}/css/lightbox.css">
<!-- font -->
<link href='//fonts.googleapis.com/css?family=Pacifico' rel='stylesheet' type='text/css'>
<link href="//fonts.googleapis.com/css?family=Raleway:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
<link href='//fonts.googleapis.com/css?family=Roboto+Condensed:400,700italic,700,400italic,300italic,300' rel='stylesheet' type='text/css'>
<!-- //font -->
<script src="{{asset('templeteBlogKucing')}}/js/jquery-1.11.1.min.js"></script>
<script src="{{asset('templeteBlogKucing')}}/js/bootstrap.js"></script>
<script type="text/javascript">
	jQuery(document).ready(function($) {
		$(".scroll").click(function(event){		
			event.preventDefault();
			$('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
		});
	});
</script> 
<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<![endif]-->
</head>
<body>
@include('layouts.banner')
	<!-- blog -->
	<div class="blog">
			<!-- container -->
			<div class="container">
				<div class="col-md-8 blog-top-left-grid">
					<div class="left-blog left-single">
						<div class="blog-left">
							<div class="single-left-left">
								<p>Posted By <a href="#">{{$blog->users->name}}</a> &nbsp;&nbsp; {{date('F d,Y', strtotime($blog->created_at))}} &nbsp;&nbsp; <a href="#">Comments (10)</a></p>
								<img src="{{asset('img').'/'.$blog->photo}}" alt="" />
							</div>
							<div class="blog-left-bottom">
								<p>
                                    {!! $blog->content !!}
								</p>
							</div>
						</div>
						<div class="response">
							<h3>Responses</h3>
						
                            @foreach ($comment as $comments)
                            <div class="media response-info">
								<div class="media-left response-text-left">
									<a href="#">
									</a>
									<h5><a href="#">{{$comments->users->name}}</a></h5>
								</div>
								<div class="media-body response-text-right">
									<p>{{$comments->content}}</p>
									<ul>
										<li>{{date('F d,Y', strtotime($comments->created_at))}}</li>
										<li><a href="javascript:;" id="reply">Reply</a></li>
									</ul>
                                    <div id="form_reply"></div>		
								</div>
								<div class="clearfix"> </div>
							</div>
							
                            @endforeach
                        
						</div>
						<div class="opinion">
							<h3>Leave your comment</h3>
							<form action="/comment" method="post">
                                @csrf
								<input type="hidden" value="{{$blog->id}}" name="blog">
								<textarea placeholder="Message" required name="content"></textarea>
								<input type="submit" value="SEND">
							</form>
						</div>
					</div>
				</div>
                @include('layouts.sidebar')
			</div>
			<!-- //container -->
	</div>
	<!-- //blog -->
	<!-- footer -->
@include('layouts.footer')
	<!-- footer -->
	<!-- copyright -->
	<div class="copyright">
		<div class="container">
			<p>© 2017 Cat Club. All rights reserved | Design by <a href="http://w3layouts.com">W3layouts</a></p>
		</div>
	</div>
	<!-- //copyright -->
	<script src="{{asset('templeteBlogKucing')}}/js/SmoothScroll.min.js"></script>
<script type="text/javascript" src="{{asset('templeteBlogKucing')}}/js/move-top.js"></script>
<script type="text/javascript" src="{{asset('templeteBlogKucing')}}/js/easing.js"></script>
	<!-- here stars scrolling icon -->
	<script type="text/javascript">
		$(document).ready(function() {
			/*
				var defaults = {
				containerID: 'toTop', // fading element id
				containerHoverID: 'toTopHover', // fading element hover id
				scrollSpeed: 1200,
				easingType: 'linear' 
				};
			*/
								
			$().UItoTop({ easingType: 'easeOutQuart' });
			$
			});
            $('body .media').on('click','#reply',function(){
                $('#form_reply').empty();   
                $('#form_reply').append(`<div class="opinion"><form action="/comment" method="post">
                                @csrf
								<input type="hidden" value="{{$blog->id}}" name="blog">
								<textarea placeholder="Message" required name="content"></textarea>
								<input type="submit" value="SEND">
							</form></div>`)
            });
	</script>
<!-- //here ends scrolling icon -->
</body>	
</html>