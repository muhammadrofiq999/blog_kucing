<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

//Halaman home
Route::get('/', 'HomeCOntroller@index');

//Halaman admin
Route::resource('blog', 'admin\BlogController')->middleware(['auth', 'admin']);
Route::resource('category', 'admin\CategoryController')->middleware(['auth', 'admin']);
Route::get('profile', 'admin\ProfileController@index')->middleware(['auth', 'admin'])->name('profile');
Route::post('profile/update', 'admin\ProfileController@update')->middleware(['auth', 'admin']);
Auth::routes();
Route::get('blogs/{id}', 'HomeController@show');
Route::get('/home', 'HomeController@index')->name('home');
// post comment
Route::post('comment', 'CommentController@store');
