<?php

namespace App\Http\Controllers;

use App\Blog;
use App\Category;
use App\Comment;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $category = Category::get();
        $latest = Blog::latest()->limit(5)->get();
        $blog = Blog::with('users', 'comments')->get();
        $comment = Comment::with('users')->latest()->limit(3)->get();
        return view('templeteBlogKucing.master', compact('category', 'blog', 'latest', 'comment'));
    }
    public function show($id)
    {
        $category = Category::get();
        $latest = Blog::latest()->limit(5)->get();
        $blog = Blog::with('users')->where('slug', $id)->first();
        $comment = Comment::with('reply', 'users')->where('blog_id', $blog->id)->get();
        return view('templeteBlogKucing.show', compact('category', 'blog', 'latest', 'comment'));
    }
}
