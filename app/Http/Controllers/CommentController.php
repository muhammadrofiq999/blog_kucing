<?php

namespace App\Http\Controllers;

use App\Comment;
use App\Reply_Comment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CommentController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function store(Request $request)
    {
        Comment::create([
            'user_id' => Auth::user()->id,
            'blog_id' => $request->blog,
            'content' => $request->content
        ]);
        return redirect()->back();
    }
    public function reply(Request $request)
    {
        Reply_Comment::create([
            'user_id' => Auth::user()->id,
            'comment_id' => $request->comment,
            'content' => $request->content
        ]);
        return redirect()->back();
    }
}
