<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ProfileController extends Controller
{
    public function index()
    {
        $id = Auth::user()->id;
        $profile = User::find($id);
        return view('admin.pages.profile', compact('profile'));
    }
    public function update(Request $request)
    {
        if ($request->passowrd) {
            User::where('id', Auth::user()->id)->update([
                'name' => $request->name,
                'email' => $request->email,
                'password' => $request->password
            ]);
        } else {
            User::where('id', Auth::user()->id)->update([
                'name' => $request->name,
                'email' => $request->email
            ]);
        }
        return redirect()->back()->with('success', 'Profil Success Updated');
    }
}
