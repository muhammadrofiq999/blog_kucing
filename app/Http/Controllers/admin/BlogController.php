<?php

namespace App\Http\Controllers\admin;

use App\Blog;
use App\Category;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Str;
use Yajra\DataTables\Facades\DataTables;

class BlogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $blog = DataTables::of(Blog::with('categories', 'users')->get())
                ->addColumn('action', function ($row) {
                    $actionBtn = '<a href="/blog/' . $row->id . '/edit" class="edit btn btn-success btn-sm">Edit</a> <a href="javascript:;" data-id="' . $row->id . '" class="delete btn btn-danger btn-sm">Delete</a>';
                    return $actionBtn;
                })
                ->rawColumns(['action'])
                ->addIndexColumn()
                ->make(true);
            return $blog;
        }
        // $blog = Blog::with('categories', 'users')->get();
        return view('admin.pages.blog');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $category = Category::get();
        return view('admin.pages.create_blog', compact('category'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // menyimpan data file yang diupload ke variabel $file
        $this->validate($request, [
            'title' => 'required',
            'categories' => 'required',
            'content' => 'required',
            'photo' => 'required'
        ]);
        $file = $request->file('photo');
        $photo = time() . "_" . $file->getClientOriginalName();
        Blog::create([
            'user_id' => Auth::user()->id,
            'categories_id' => $request->categories,
            'title' => $request->title,
            'slug' => Str::slug($request->title, '-'),
            'content' => $request->content,
            'photo' => $photo
        ]);
        // isi dengan nama folder tempat kemana file diupload
        $tujuan_upload = 'img';
        $file->move($tujuan_upload, $photo);
        return redirect()->to('/blog')
            ->with('success', 'Blog created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $blog = Blog::where('id', $id)->first();
        $category = Category::get();
        return view('admin.pages.edit_blog', compact('category', 'blog'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $blog = Blog::where('id', $id)->first();
        $this->validate($request, [
            'title' => 'required',
            'categories' => 'required',
            'content' => 'required',
        ]);
        $file = $request->file('photo');
        if ($file) {
            $photo = time() . "_" . $file->getClientOriginalName();
            Blog::where('id', $id)->update([
                'categories_id' => $request->categories,
                'title' => $request->title,
                'slug' => Str::slug($request->title, '-'),
                'content' => $request->content,
                'photo' => $photo
            ]);
            // isi dengan nama folder tempat kemana file diupload
            $tujuan_upload = 'img';
            $file->move($tujuan_upload, $photo);
            File::delete('img/' . $blog->photo);
        } else {
            Blog::where('id', $id)->update([
                'categories_id' => $request->categories,
                'title' => $request->title,
                'slug' => Str::slug($request->title, '-'),
                'content' => $request->content
            ]);
        }

        return redirect()->to('/blog')
            ->with('success', 'Blog edited successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Blog::find($id)->delete();
        return response()->json(['success' => 'Blog deleted!']);
        // return redirect('/blog');
    }
}
