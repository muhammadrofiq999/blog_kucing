<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reply_Comment extends Model
{
    protected $table = 'reply_comment';
    protected $fillable = [
        'user_id', 'comment_id', 'content'
    ];
    public function users()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
    public function comments()
    {
        return $this->belongsTo(Comment::class, 'comment_id', 'id');
    }
}
