<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected $table = 'comment';
    protected $fillable = [
        'user_id', 'blog_id', 'content'
    ];
    public function blog()
    {
        return $this->belongsTo(Blog::class, 'blog_id', 'id');
    }
    public function reply()
    {
        return $this->hasMany(Reply_Comment::class);
    }
    public function users()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
}
